// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class FLAPPYBIRD_API AMainMenuPlayerControllerBase : public APlayerController
{
	GENERATED_BODY()

	/**
	 * @brief Constructor
	 */
	AMainMenuPlayerControllerBase();

	/////////Methods/////////
public:
	//Begining when game will started.
	void BeginPlay() override;

	////////Fields//////////
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FlappyWidgets")
	TSubclassOf<UUserWidget> ClassOfMainMenuWidget;
	UPROPERTY()
	UUserWidget* MainMenuWidget;
};
