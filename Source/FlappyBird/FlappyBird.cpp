// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FlappyBird.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FlappyBird, "FlappyBird" );
DEFINE_LOG_CATEGORY(LogFlappyBirdGame);