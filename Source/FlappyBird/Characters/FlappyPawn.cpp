// Fill out your copyright notice in the Description page of Project Settings.


#include "FlappyPawn.h"
#include "Engine/Engine.h"


// Sets default values
AFlappyPawn::AFlappyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFlappyPawn::BeginPlay()
{
	Super::BeginPlay();

	CurGameMode = Cast<AFlappyBirdGameModeBase>(GetWorld()->GetAuthGameMode());
	CurController = Cast<AFlappyPlayerControllerBase>(GetController());
}

void AFlappyPawn::OnCompOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	OtherComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (CurController)
	{
		CurController->UpdateScoreInformation();
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController is null!!"));
	}

	if (PointSound)
	{
		PointSound->Play();
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("PointSound is null!!"));
	}
}

void AFlappyPawn::OnCompHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (bFlappyIsFallen)
		return;

	if (CurController)
	{
		CurController->LoseWidgetVisibility();
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController is null!!"));
	}


	for (auto tag : OtherComp->ComponentTags)
	{
		if (tag == FName(TEXT("Barrier")))
		{
			if (CurGameMode == nullptr)
			{
				UE_LOG(LogFlappyBirdGame, Log, TEXT("CurGameMode is null!!"));
				return;
			}	

			//Flappy initialization, now can't fly
			bFlappyIsFallen = true;
			Flappy->Stop();
			Flappy->BodyInstance.bLockYRotation = true;

			//Segment initialization
			CurGameMode->SetSpeedOfSegment(0);
			ABaseSegment* CurSegment = Cast<ABaseSegment>(OtherActor);

			if (CurSegment == nullptr)
			{
				UE_LOG(LogFlappyBirdGame, Log, TEXT("CurSegment is null!!"));
				return;
			}

			TArray<FBlock> ArrOfBlocks = CurSegment->GetPipeBlocks();

			if (ArrOfBlocks.Num() == 0)
			{
				UE_LOG(LogFlappyBirdGame, Log, TEXT("ArrOfBlocks size is null!!"));
				return;
			}

			for (auto block : ArrOfBlocks)  //Off collision on all pipe blocks on scene
			{
				block.UpPipe->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				block.DownPipe->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			}


			//Audio initialization
			if (HitSound)
			{
				HitSound->Play();
				bHitSoundPlayed = true;
			}

			if (DieSound)
			{
				DieSound->Play();
			}

			break;
		}


		if (tag == FName(TEXT("Ground")))
		{
			if (CurGameMode == nullptr)
			{
				UE_LOG(LogFlappyBirdGame, Log, TEXT("CurGameMode is null!!"));
				return;
			}

			//Flappy initialization
			bFlappyIsFallen = true;
			Flappy->Stop();
			Flappy->BodyInstance.bLockYRotation = true;

			//Segment initialization
			CurGameMode->SetSpeedOfSegment(0);

			//Sound initialization
			if (!bHitSoundPlayed)
			{
				HitSound->Play();
			}

			if (DieSound->IsPlaying())
			{
				DieSound->Stop();
			}

			break;
		}
	}

	CurGameMode->PlayDieMusic();
}

// Called every frame
void AFlappyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bUpdatedVolume)    //������� ��������� � ���, ��� ������������ ������ � BP �������� �������� � � ����� ������ update(�) ������ ���������� null error, �������� �������, ���� ������� ���������� audio �������.
	{
		if (UpdateSoundVolume() && UpdateMusicVolume())
		{
			bUpdatedVolume = true;
		}
	}


	if (!bFlappyIsFallen && bFirstClicked)
	{
		Flappy->AddRelativeRotation(FRotator(SpeedRotate * DeltaTime, 0, 0));
	}
}

// Called to bind functionality to input
void AFlappyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AFlappyPawn::Fly);
	FInputActionBinding& EscapeAction = PlayerInputComponent->BindAction("Escape", EInputEvent::IE_Pressed, this, &AFlappyPawn::SetEscapeMenuVisibility);
	EscapeAction.bExecuteWhenPaused = true;
}


void AFlappyPawn::Fly()
{
	if (!bFirstClicked)
	{
		CollisionInitialization();
		CurGameMode->CreateSegment();
		CurController->QuickHelpWidgetVisibility();

		bFirstClicked = true;
	}


	if (!bFlappyIsFallen)
	{
		if (Flappy->GetComponentLocation().Z <= UpperFlightLimit)
		{
			Flappy->SetPhysicsLinearVelocity(FVector(0, 0, Flap));
			Flappy->SetRelativeRotation(FRotator(StartRotate, 0, 0));
		}

		if (WingSound)
		{
			WingSound->Play();
		}
	}
}

void AFlappyPawn::CollisionInitialization()
{
	if (Flappy != nullptr)
	{
		Flappy->SetSimulatePhysics(true);
		Flappy->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
		Flappy->OnComponentHit.AddDynamic(this, &AFlappyPawn::OnCompHit);
		Flappy->OnComponentBeginOverlap.AddDynamic(this, &AFlappyPawn::OnCompOverlap);
	}
	UE_LOG(LogFlappyBirdGame, Log, TEXT("Flappy is null!!"));
}

void AFlappyPawn::SetEscapeMenuVisibility()
{
	if (CurController)
	{
		CurController->EscapeWidgetVisibility();
	}

	UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController is null!!"));
}

bool AFlappyPawn::UpdateSoundVolume()
{
	if (CurController)
	{
		if (PointSound)
		{
			PointSound->SetVolumeMultiplier(CurController->GetSoundVolumeFunc());
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("PointSound is null!!"));
			return false;
		}

		if (WingSound)
		{
			WingSound->SetVolumeMultiplier(CurController->GetSoundVolumeFunc());
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("WingSound is null!!"));
			return false;
		}

		if (HitSound)
		{
			HitSound->SetVolumeMultiplier(CurController->GetSoundVolumeFunc());
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("HitSound is null!!"));
			return false;
		}

		if (DieSound)
		{
			DieSound->SetVolumeMultiplier(CurController->GetSoundVolumeFunc());
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("DieSound is null!!"));
			return false;
		}

		return true;
	}

	UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController is null!!"));
	return false;


}

bool AFlappyPawn::UpdateMusicVolume()
{
	if (CurGameMode != nullptr)
	{
		if (CurController != nullptr)
		{
			if (CurGameMode->SetMusicVolume(CurController->GetMusicVolumeFunc()))
			{
				return true;
			}

			UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController->GetMusic return false!!"));
			return false;

		}

		UE_LOG(LogFlappyBirdGame, Log, TEXT("CurController is null!!"));
		return false;
	}

	UE_LOG(LogFlappyBirdGame, Log, TEXT("CurGameMode is null!!"));
	return false;
}


