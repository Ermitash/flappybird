// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseSegment.h"

// Sets default values
ABaseSegment::ABaseSegment()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FSegmentProperties ABaseSegment::GetProperty() const
{
	return SegmentProperties;
}

// Called when the game starts or when spawned
void ABaseSegment::BeginPlay()
{
	Super::BeginPlay();
	InitBlocks();
}

void ABaseSegment::InitBlock(FBlock* Block)
{
	FMath::SRand();
	Block->UpPipe->AddWorldOffset(FVector(0, 0, FMath::FRandRange(MinHeight, MaxHeight)));
	Block->DownPipe->SetWorldLocation(Block->UpPipe->GetComponentLocation());
	Block->DownPipe->AddWorldOffset(FVector(0, 0, -DistanceBetweenBlocks));
	Block->BoxCollider->SetBoxExtent(FVector(Block->WidthOfPipe / 2, YWidth, DistanceBetweenBlocks / 2));
	Block->BoxCollider->SetWorldLocation(Block->DownPipe->GetComponentLocation() + FVector(0, 0, DistanceBetweenBlocks / 2));


	if (bCanMove)
	{
		Block->VerticalSpeed = FMath::FRandRange(MinVerticalSpeed, MaxVerticalSpeed);
	}
	else
	{
		Block->VerticalSpeed = 0;
	}
}

void ABaseSegment::InitBlocks()
{
	if (bMovableBlocks)
	{
		bCanMove = static_cast<bool>(FMath::RandRange(0, 1));
	}
	else
	{
		bCanMove = false;
	}


	for (int32 i = 0; i < PipeBlocksArray.Num(); i++)
	{
		InitBlock(&PipeBlocksArray[i]);
	}
}

// Called every frame
void ABaseSegment::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bCanMove)
	{
		MoveBlocks(DeltaTime);
	}

}

TArray<FBlock> ABaseSegment::GetPipeBlocks() const
{
	return PipeBlocksArray;
}

void ABaseSegment::MoveBlocks(float DeltaTime)
{
	for (int32 i = 0; i < PipeBlocksArray.Num(); i++)
	{
		if (PipeBlocksArray[i].UpPipe->GetRelativeTransform().GetLocation().Z >= UpBorderOfBlockMovement ||
			PipeBlocksArray[i].DownPipe->GetRelativeTransform().GetLocation().Z <= DownBorderOfBlockMovement)
		{
			PipeBlocksArray[i].Direction *= -1;
		}

		PipeBlocksArray[i].UpPipe->AddRelativeLocation(FVector(0, 0, PipeBlocksArray[i].Direction * PipeBlocksArray[i].VerticalSpeed * DeltaTime));
		PipeBlocksArray[i].DownPipe->AddRelativeLocation(FVector(0, 0, PipeBlocksArray[i].Direction * PipeBlocksArray[i].VerticalSpeed * DeltaTime));
		PipeBlocksArray[i].BoxCollider->AddRelativeLocation(FVector(0, 0, PipeBlocksArray[i].Direction * PipeBlocksArray[i].VerticalSpeed * DeltaTime));
	}
}

