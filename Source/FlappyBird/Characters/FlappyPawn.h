// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/InputComponent.h"
#include "GameFramework/Pawn.h"
#include "PaperFlipbookComponent.h"
#include "FlappyBird/GameModes/FlappyBirdGameModeBase.h"
#include "FlappyBird/Actors/Segment/BaseSegment.h"
#include "FlappyBird/PlayerControllers/FlappyPlayerControllerBase.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "FlappyPawn.generated.h"

UCLASS()
class FLAPPYBIRD_API AFlappyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AFlappyPawn();

	// Called every frame
	 void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/**
	 * @brief Fly on click
	 */
	void Fly();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/**
	 * @brief Disable collision of other component and add one point to all scores
	 * @param OverlappedComp    this overlapped component
	 * @param OtherActor        Actor that hit
	 * @param OtherComp         Component that hit
	 * @param OtherBodyIndex    A body index that got hit
	 * @param bFromSweep        Is it hit from sweep function (collision detection function, not sure if this include line traces)
	 * @param SweepResult       If yes, this contains result data of that sweep
	 */
	UFUNCTION()
	void OnCompOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/**
	 * @brief 
	 * @param OverlappedComp this overlapped component
	 * @param OtherActor Actor that hit
	 * @param OtherComp  Component that hit
	 * @param NormalImpulse Impulse of hit
	 * @param Hit other hit results
	 */
	UFUNCTION()
	void OnCompHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/**
	 * @brief Initialize collision of Flappy on start game.
	 */
	void CollisionInitialization();

	/**
	 * @brief Call escape menu from player controller
	 */
	void SetEscapeMenuVisibility();

	/**
	 * @brief Update sound volume
	 * @return Is success update volume or not
	 */
	UFUNCTION(BlueprintCallable)
	bool UpdateSoundVolume();

	/**
	 * @brief Update music volume
	 * @return Is success update music volume or not
	 */
	UFUNCTION(BlueprintCallable)
	bool UpdateMusicVolume();

	/////////Fields/////////
protected:
	UPROPERTY()
	AFlappyBirdGameModeBase* CurGameMode;
	UPROPERTY()
	AFlappyPlayerControllerBase* CurController;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FlappyProperties")
	UPaperFlipbookComponent* Flappy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlappyProperties")
	float Flap = 350.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlappyProperties")
	float SpeedRotate = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FlappyProperties")
	float StartRotate = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float UpperFlightLimit = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ScorePoints = 0;

	bool bFlappyIsFallen = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AudioProperties")
	UAudioComponent* PointSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AudioProperties")
	UAudioComponent* WingSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AudioProperties")
	UAudioComponent* HitSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AudioProperties")
	UAudioComponent* DieSound;

	bool bHitSoundPlayed = false;
	bool bFirstClicked = false;
	bool bUpdatedVolume = false;
};
