// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <PaperSpriteComponent.h>
#include <Components/BoxComponent.h>
#include "BaseSegment.generated.h"

/*
* The block consisting of the top and bottom pipe, also consider collision for increasing points
*/
USTRUCT(BlueprintType)
struct FBlock
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Block")
	UPaperSpriteComponent* UpPipe;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Block")
	UPaperSpriteComponent* DownPipe;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Block")
	UBoxComponent* BoxCollider;

	float WidthOfPipe = 128.0f;
	int8 Direction = 1;
	float VerticalSpeed;

};


/*
* Property of segment
*/
USTRUCT(BlueprintType)
struct FSegmentProperties
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Width = 1920.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Height = 1080.0f;
};


UCLASS()
class FLAPPYBIRD_API ABaseSegment : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseSegment();

	/*
	* Get property of block
	* @return FProperties
	*/
    FSegmentProperties GetProperty() const;

	////////////////////Methods////////////////////////
protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

	/*
	* Initialization parametrs only for one block
	* @param block current block
	*/
	void InitBlock(FBlock* block);

	/*
	* Initialization parametrs for all block
	* @param block current block
	*/
	void InitBlocks();

	/*
	 * Move blocks
	 * @DeltaTime time between two frames
	*/
	void MoveBlocks(float DeltaTime);
public:
	// Called every frame
	 void Tick(float DeltaTime) override;

	/**
	 * @brief Get array of pipe blocks
	 */
	 TArray<FBlock> GetPipeBlocks() const;
	///////////////////Fields//////////////////////////
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float MaxHeight = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float MinHeight = -250.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float UpBorderOfBlockMovement = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float DownBorderOfBlockMovement = -400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float MinVerticalSpeed = -100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float MaxVerticalSpeed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float DistanceBetweenBlocks = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	bool bMovableBlocks;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	TArray<FBlock> PipeBlocksArray;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	FSegmentProperties SegmentProperties;
	

	bool bCanMove;
	float YWidth = 20.0f;
};
