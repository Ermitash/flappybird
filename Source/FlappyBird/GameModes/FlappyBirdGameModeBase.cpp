// Fill out your copyright notice in the Description page of Project Settings.


#include "FlappyBirdGameModeBase.h"
#include "Engine/Engine.h"

AFlappyBirdGameModeBase::AFlappyBirdGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AFlappyBirdGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

void AFlappyBirdGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveSegments(DeltaTime);
}

void AFlappyBirdGameModeBase::CreateSegment()
{
	static uint32 NumberOfSegment = 0;
	auto ClassOfSegment = LevelArray[CurrentLevel].Segments[FMath::RandRange(0, LevelArray[CurrentLevel].Segments.Num() - 1)];

	if(ClassOfSegment == nullptr)
	{
		UE_LOG(LogFlappyBirdGame, Error, TEXT("ClassOfSegment was not created!!"));
		return;
	}

	auto ObjectOfSegment = Cast<ABaseSegment>(GetWorld()->SpawnActor(ClassOfSegment));

	if (ObjectOfSegment == nullptr)
	{
		UE_LOG(LogFlappyBirdGame, Error, TEXT("ObjectOfSegment was not created!!"));
		return;
	}

	ObjectOfSegment->SetActorLocation(FVector(WidthOfSpace/2 + ObjectOfSegment->GetProperty().Width / 2, 0, 0));
	SegmentsOnLevel.Add(ObjectOfSegment);

	if (NumberOfSegment == TNumericLimits<uint32>::Max())
	{
		NumberOfSegment = 0;
	}
	else
	{
		++NumberOfSegment;
	}

	++SpawnedSegmentsOnCurrentLevel;
	if (SpawnedSegmentsOnCurrentLevel >= LevelArray[CurrentLevel].LenghtOfLevel)
	{
		if (CurrentLevel < LevelArray.Num() - 1)
		{
			++CurrentLevel;
		}

		SpawnedSegmentsOnCurrentLevel = 0;
	}
}

void AFlappyBirdGameModeBase::MoveSegments(float DeltaTime)
{
	if (SegmentsOnLevel.Num() > 0)
	{
		for (auto& segment : SegmentsOnLevel)
		{
			segment->AddActorWorldOffset(FVector(SpeedOfSegment * DeltaTime, 0, 0));
		}
		
		if (SegmentsOnLevel.Last()->GetActorLocation().X <= WidthOfSpace / 2 - SegmentsOnLevel.Last()->GetProperty().Width / 2)
		{
			CreateSegment();
		}

		if (SegmentsOnLevel[0]->GetActorLocation().X <= -WidthOfSpace / 2 - SegmentsOnLevel.Last()->GetProperty().Width / 2)
		{
			DestroySegment();
		}
	}
}

void AFlappyBirdGameModeBase::DestroySegment()
{
	SegmentsOnLevel[0]->Destroy();
	SegmentsOnLevel.RemoveAt(0);
}

void AFlappyBirdGameModeBase::SetGamePaused(bool bIsPaused)
 {
     APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
     if (MyPlayer != nullptr)
     {
         MyPlayer->SetPause(bIsPaused);
     }
 }

void AFlappyBirdGameModeBase::PlayDieMusic()
{
	if(BackgroundMusic != nullptr)
	{
		BackgroundMusic->Stop();
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("BackgroundMusic is null!!"));
		return;
	}

	if(DieMusic != nullptr)
	{
		DieMusic->Play();
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("DieMusic is null!!"));
	}
	
}

bool AFlappyBirdGameModeBase::SetMusicVolume(float Volume)
{
	if(BackgroundMusic != nullptr)
	{
		BackgroundMusic->SetVolumeMultiplier(Volume);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("BackgroundMusic is null!!"));
		return false;
	}

	if(DieMusic != nullptr)
	{
		DieMusic->SetVolumeMultiplier(Volume);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("DieMusic is null!!"));
		return false;
	}

	return true;
}
