// Fill out your copyright notice in the Description page of Project Settings.


#include "FlappyPlayerControllerBase.h"
#include "UserWidget.h"
#include "Engine/Engine.h"
#include "SWindow.h"
#include "Kismet/GameplayStatics.h"

AFlappyPlayerControllerBase::AFlappyPlayerControllerBase()
{

}

void AFlappyPlayerControllerBase::BeginPlay()
{
	Super::BeginPlay();

	if (ClassOfScoreWidget)
	{
		ScoreWidget = CreateWidget<UUserWidget>(this, ClassOfScoreWidget);

		if (ScoreWidget)
		{
			ScoreWidget->AddToViewport(0);

			FInputModeGameAndUI GameMode;
			GameMode.SetWidgetToFocus(ScoreWidget->TakeWidget());
			GameMode.SetHideCursorDuringCapture(false);
			SetInputMode(GameMode);
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("ScoreWidget is null!"));
			return;
		}
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("ClassOfScoreWidget is null!"));
		return;
	}

	if (ClassOfQuickHelpWidget)
	{
		QuickHelpWidget = CreateWidget<UUserWidget>(this, ClassOfQuickHelpWidget);

		if (QuickHelpWidget)
		{
			QuickHelpWidget->AddToViewport();
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("QuickHelpWidget is null!"));
			return;
		}
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("ClassOfQuickHelpWidget is null!"));
		return;
	}

	if (ClassOfLoseWidget)
	{
		LoseWidget = CreateWidget<UUserWidget>(this, ClassOfLoseWidget);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("ClassOfLoseWidget is null!"));
		return;
	}

	if (ClassOfEscapeWidget)
	{
		EscapeWidget = CreateWidget<UUserWidget>(this, ClassOfEscapeWidget);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("ClassOfEscapeWidget is null!"));
		return;
	}

	LoadFlappyGame();
	UpdateScoreWidgetInformation();
}

void AFlappyPlayerControllerBase::SetScorePoints(int32 Points)
{
	ScorePoints = Points;
}

void AFlappyPlayerControllerBase::AddScorePoint()
{
	++ScorePoints;
}

void AFlappyPlayerControllerBase::UpdateScoreInformation()
{
	AddScorePoint();

	if (GetScorePointsFunc() > GetHighScorePointsFunc())
	{
		SetHighScorePointsFunc(GetScorePointsFunc());
	}

	UpdateScoreWidgetInformation();
}

int32 AFlappyPlayerControllerBase::GetScorePointsFunc() const
{
	return ScorePoints;
}

void AFlappyPlayerControllerBase::SetHighScorePointsFunc(int32 Points)
{
	HighScorePoints = Points;
}

int32 AFlappyPlayerControllerBase::GetHighScorePointsFunc() const
{
	return  HighScorePoints;
}

void AFlappyPlayerControllerBase::UpdateScoreWidgetInformation()
{
	if (ScoreWidget != nullptr)
	{
		FOutputDeviceNull Device;
		FString CallUpdateScoreFunc = "UpdateScore " + FString::FromInt(ScorePoints);
		FString CallUpdateHighScoreFunc = "UpdateHighScore " + FString::FromInt(HighScorePoints);

		ScoreWidget->CallFunctionByNameWithArguments(*CallUpdateScoreFunc, Device, this, true);
		ScoreWidget->CallFunctionByNameWithArguments(*CallUpdateHighScoreFunc, Device, this, true);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("ScoreWidget is null!"));
	}
}

void AFlappyPlayerControllerBase::LoseWidgetVisibility()
{
	if (LoseWidget != nullptr)
	{
		LoseWidget->AddToViewport(1);
		bShowMouseCursor = true;

		FInputModeGameAndUI GameMode;
		GameMode.SetWidgetToFocus(LoseWidget->TakeWidget());
		SetInputMode(GameMode);

		int32 WidthViewport = 0;
		int32 HeightViewport = 0;
		GetViewportSize(WidthViewport, HeightViewport);
		SetMouseLocation(WidthViewport / 2 + 1, HeightViewport / 2 + 1);
		SetMouseLocation(WidthViewport / 2, HeightViewport / 2);

		FOutputDeviceNull Device;
		FString CallUpdateScoreFunc = "UpdateScore " + FString::FromInt(ScorePoints);
		FString CallUpdateHighScoreFunc = "UpdateHighScore " + FString::FromInt(HighScorePoints);

		LoseWidget->CallFunctionByNameWithArguments(*CallUpdateScoreFunc, Device, this, true);
		LoseWidget->CallFunctionByNameWithArguments(*CallUpdateHighScoreFunc, Device, this, true);
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("LoseWidget is null!"));
	}
}

void AFlappyPlayerControllerBase::EscapeWidgetVisibility()
{
	if (bShowEscapeWidget)
	{
		if (EscapeWidget != nullptr)
		{
			EscapeWidget->AddToViewport(2);

			FInputModeGameAndUI GameMode;
			GameMode.SetWidgetToFocus(EscapeWidget->TakeWidget());
			SetInputMode(GameMode);

			if (!bShowMouseCursor)
			{
				bShowMouseCursor = true;
				int32 WidthViewport = 0;
				int32 HeightViewport = 0;
				GetViewportSize(WidthViewport, HeightViewport);	
				SetMouseLocation(WidthViewport / 2 + 1, HeightViewport / 2 + 1);
				SetMouseLocation(WidthViewport / 2, HeightViewport / 2);
			}

			FOutputDeviceNull Device;
			FString CallUpdateEscapeMenu = "UpdateEscapeMenu " + FString::FromInt(ScorePoints) + " " + FString::FromInt(HighScorePoints) + " " + FString::SanitizeFloat(SoundVolume) + " " + FString::SanitizeFloat(MusicVolume);

			EscapeWidget->CallFunctionByNameWithArguments(*CallUpdateEscapeMenu, Device, this, true);

			SetPause(true);
			bShowEscapeWidget = false;
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("EscapeWidget is null!"));
		}

	}
	else
	{
		if (EscapeWidget != nullptr)
		{
			if (EscapeWidget->IsInViewport())
			{
				EscapeWidget->RemoveFromViewport();

				if (LoseWidget != nullptr)
				{
					if (!LoseWidget->IsInViewport())
					{
						bShowMouseCursor = false;
						int32 WidthViewport = 0;
						int32 HeightViewport = 0;
						GetViewportSize(WidthViewport, HeightViewport);
						SetMouseLocation(WidthViewport / 2, HeightViewport / 2);
					}
				}
				else
				{
					UE_LOG(LogFlappyBirdGame, Log, TEXT("LoseWidget is null!"));
				}

				FInputModeGameAndUI GameMode;
				GameMode.SetWidgetToFocus(ScoreWidget->TakeWidget());
				SetInputMode(GameMode);

				SetPause(false);
				bShowEscapeWidget = true;
			}
		}
		else
		{
			UE_LOG(LogFlappyBirdGame, Log, TEXT("EscapeWidget is null!"));
		}
	}
}

void AFlappyPlayerControllerBase::LoadFlappyGame()
{
	UFlappySaveGameBase* LoadGameInstance = Cast<UFlappySaveGameBase>(UGameplayStatics::CreateSaveGameObject(UFlappySaveGameBase::StaticClass()));
	LoadGameInstance = Cast<UFlappySaveGameBase>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->SaveSlotName, LoadGameInstance->UserIndex));

	if (LoadGameInstance != nullptr)
	{
		SetHighScorePointsFunc(LoadGameInstance->HighScore);
		SoundVolume = LoadGameInstance->SoundVolume;
		MusicVolume = LoadGameInstance->MusicVolume;
	}
	else
	{
		UE_LOG(LogFlappyBirdGame, Log, TEXT("LoadGameInstance is null"));
	}
}

void AFlappyPlayerControllerBase::SaveFlappyGame()
{
	UFlappySaveGameBase* SaveGameInstance = Cast<UFlappySaveGameBase>(UGameplayStatics::CreateSaveGameObject(UFlappySaveGameBase::StaticClass()));

	SaveGameInstance->HighScore = GetHighScorePointsFunc();
	SaveGameInstance->MusicVolume = MusicVolume;
	SaveGameInstance->SoundVolume = SoundVolume;

	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
}

void AFlappyPlayerControllerBase::SetMusicVolumeFunc(float Volume)
{
	MusicVolume = Volume;
}

void AFlappyPlayerControllerBase::SetSoundVolumeFunc(float Volume)
{
	SoundVolume = Volume;
}


float AFlappyPlayerControllerBase::GetMusicVolumeFunc() const
{
	return MusicVolume;
}

float AFlappyPlayerControllerBase::GetSoundVolumeFunc() const
{
	return SoundVolume;
}

void AFlappyPlayerControllerBase::QuickHelpWidgetVisibility()
{
	if (QuickHelpWidget && QuickHelpWidget->IsInViewport())
	{
		QuickHelpWidget->RemoveFromParent();
	}

}

void AFlappyPlayerControllerBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	SaveFlappyGame();
}
