// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuPlayerControllerBase.h"
#include "UserWidget.h"

AMainMenuPlayerControllerBase::AMainMenuPlayerControllerBase()
{
}

void AMainMenuPlayerControllerBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (ClassOfMainMenuWidget)
	{
	    MainMenuWidget = CreateWidget<UUserWidget>(this, ClassOfMainMenuWidget);

		if (MainMenuWidget)
		{
			MainMenuWidget->AddToViewport();

			FInputModeUIOnly GameMode;
		    GameMode.SetWidgetToFocus(MainMenuWidget->TakeWidget());
		    SetInputMode(GameMode);
		}
	}
}
