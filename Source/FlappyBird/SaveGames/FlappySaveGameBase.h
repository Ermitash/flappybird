// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "FlappySaveGameBase.generated.h"

/**
 * 
 */
UCLASS()
class FLAPPYBIRD_API UFlappySaveGameBase : public USaveGame
{
	GENERATED_BODY()

	//Constructor
	UFlappySaveGameBase();

	//////Fields//////
public:
	UPROPERTY(BlueprintReadWrite, Category = "FlappySaveGame")
	int32 HighScore;

	UPROPERTY(BlueprintReadWrite, Category = "FlappySaveGame")
    FString SaveSlotName;

    UPROPERTY(BlueprintReadWrite, Category = "FlappySaveGame")
    int32 UserIndex;

	UPROPERTY(BlueprintReadWrite, Category = "FlappySaveGame")
    float SoundVolume;

	UPROPERTY(BlueprintReadWrite, Category = "FlappySaveGame")
    float MusicVolume;
};
