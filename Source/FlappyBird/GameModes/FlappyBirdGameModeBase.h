// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FlappyBird/Actors/Segment/BaseSegment.h"
#include "GameFramework/GameModeBase.h"
#include "FlappyBird.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "FlappyBirdGameModeBase.generated.h"


USTRUCT(BlueprintType)
struct FLevelFlappyBird
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ABaseSegment>> Segments;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 LenghtOfLevel;      //in segments
};

UCLASS()
class FLAPPYBIRD_API AFlappyBirdGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	/*
	 * Constructor
	 */
	AFlappyBirdGameModeBase();

	///////////////Methods///////////////
public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every tick
	virtual void Tick(float DeltaTime) override;

	/*
	* Create segment
	* @return void
	*/
	void CreateSegment();

	/*
	* Move every segment
	* float DeltaTime time between ticks
	* @return void
	*/
	void MoveSegments(float DeltaTime);

	/*
	* Destroy segment if it outside block
	* @return void
	*/
	void DestroySegment();

public:

	void SetGamePaused(bool bIsPaused);

	void SetSpeedOfSegment(float SpeedOfSegment)
	{
		this->SpeedOfSegment = SpeedOfSegment;
	}

	float GetSpeedOfSegment() const
	{
		return SpeedOfSegment;
	}

	void PlayDieMusic();

	bool SetMusicVolume(float Volume);

	//////////////Fields////////////////
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpeedOfSegment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FLevelFlappyBird> LevelArray;

	TArray<ABaseSegment*> SegmentsOnLevel;
	int8 CurrentLevel;
	int8 SpawnedSegmentsOnCurrentLevel = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property of space")
	int32 WidthOfSpace = 1920;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Property of space")
	int32 HeightOfSpace = 1080;
	int32 HalfOfWidthOfSpace;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAudioComponent* BackgroundMusic;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAudioComponent* DieMusic;
};
