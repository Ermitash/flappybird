// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/Core/Public/Misc/OutputDeviceNull.h"
#include "SaveGames/FlappySaveGameBase.h"
#include "FlappyBird/FlappyBird.h"
#include "FlappyPlayerControllerBase.generated.h"

/**
 * A controller that is responsible for controlling widgets, points, sound
 */
UCLASS()
class FLAPPYBIRD_API AFlappyPlayerControllerBase : public APlayerController
{
	GENERATED_BODY()

	/**
	 * @brief Constructor
	 */
	AFlappyPlayerControllerBase();
	/////////Methods/////////
public:
	//Started then when the game begins
	void BeginPlay() override;

	/**
	 * @brief Set score points
	 * @param Points points count
	 */
	void SetScorePoints(int32 Points);

	/**
	 * @brief Get current score points
	 */
	int32 GetScorePointsFunc() const;

	/**
	 * @brief add one point to all score points
	 */
	void AddScorePoint();

	/**
	 * @brief Update score and widget information about it
	 */
	void UpdateScoreInformation();
	
	/**
	 * @brief Set high score
	 * @param Points Current points
	 */
	void SetHighScorePointsFunc(int32 Points);

	/**
	 * @brief Get high score
	 */
	int32 GetHighScorePointsFunc() const;

	/**
	 * @brief Update all widgets who connected with score.
	 */
	void UpdateScoreWidgetInformation();

	/**
	 * @brief Set lose widget visibility
	 */
	void LoseWidgetVisibility();

	/**
	 * @brief Set escape widget visibility
	 */
	void EscapeWidgetVisibility();

	/**
	 * @brief Set quick help widget visibility
	 */
	void QuickHelpWidgetVisibility();

	/**
	 * @brief Load flappy game parameters
	 */
	void LoadFlappyGame();

	/**
	 * @brief Save flappy game parameters
	 */
	void SaveFlappyGame();

	/**
	 * @brief Set volume of music
	 * @param Volume Volume of Music
	 */
	UFUNCTION(BlueprintCallable)
	void SetMusicVolumeFunc(float Volume);

	/**
	 * @brief Get volume of music
	 */
	float GetMusicVolumeFunc() const;

	/**
	 * @brief Set volume of sound
	 * @param Volume Volume of sound
	 */
	UFUNCTION(BlueprintCallable)
	void SetSoundVolumeFunc(float Volume);

	/**
	 * @brief Get volume of sound
	 */
	float GetSoundVolumeFunc() const;

	/**
	 * @brief Event when game will closed 
	 * @param EndPlayReason Reason of close
	 */
	UFUNCTION()
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	////////Fields//////////
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> ClassOfScoreWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> ClassOfLoseWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> ClassOfEscapeWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> ClassOfQuickHelpWidget;
	UPROPERTY()
	UUserWidget* LoseWidget;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UUserWidget* ScoreWidget;
	UPROPERTY()
	UUserWidget* EscapeWidget;
	UPROPERTY()
	UUserWidget* QuickHelpWidget;

	int32 ScorePoints;
	int32 HighScorePoints;
	float MusicVolume = 0.1f;
	float SoundVolume = 0.9f;
	bool bShowEscapeWidget = true;
};
